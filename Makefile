#!/usr/bin/make -f

WHICH_EMACS:=$(shell which emacs >/dev/null 2>&1 || echo "fail")

ELC_FILES=
ifeq ($(strip $(WHICH_EMACS)),)
ELC_FILES=$(patsubst %.el,%.elc,$(wildcard *.el)) org-ref.elc doi-utils.elc pubmed.elc
endif

all: $(ELC_FILES)

EMACS=emacs

%.elc: %.el
	$(EMACS) -q -no-site-file -L . -L .. -L ../helm --no-site-file -batch -f batch-byte-compile $<;


%.el: %.org
	$(EMACS) -batch -q -no-site-file --visit $< --funcall org-babel-tangle


